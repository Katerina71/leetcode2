from typing import List


class Solution:
    def removeDuplicates(self, nums: List[int]) -> str:
        listToStr = ' '.join([f"{elem}," for elem in nums])
        return listToStr


if __name__ == '__main__':
    nums = [3, 2, 4]
    print(Solution().removeDuplicates(nums))
