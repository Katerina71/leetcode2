class Solution:
    def isUgly(self, num: int) -> bool:
        if not num:
            return False
        for number in [2, 3, 5]:
            while not num % number:
                num //= number
        return True if num == 1 else False


if __name__ == '__main__':
    print(Solution().isUgly(0))