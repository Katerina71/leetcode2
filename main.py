# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


def check_list(list):
    j = -1
    for i in range(len(list)//2):
        list[j], list[i] = list[i], list[j]
        j -= 1
    return list


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print(check_list([]))

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
