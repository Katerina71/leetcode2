from typing import List


class Solution:
    def replaceElements(self, arr: List[int]) -> List[int]:
        i = len(arr) - 1
        mx = arr[i]
        arr[i] = -1

        while i > 0:
            i -= 1
            arr[i], mx = mx, max(mx, arr[i])

        return arr


if __name__ == '__main__':
    print(Solution().replaceElements([17, 18, 5, 4, 6, 1]))
