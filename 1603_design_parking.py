class ParkingSystem:

    def __init__(self, big: int, medium: int, small: int):
        self.size = {1: big,
                     2: medium,
                     3: small}

    def addCar(self, carType: int) -> bool:
        if self.size[carType] > 0:
            self.size[carType] -= 1
            return True
        return False


if __name__ == '__main__':
    # Your ParkingSystem object will be instantiated and called as such:
    obj = ParkingSystem(1, 1, 0)
    param_1 = obj.addCar(1)
    print(param_1)
