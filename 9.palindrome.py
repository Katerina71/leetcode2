class Solution:
    def isPalindrome(self, x: int) -> bool:
        if x < 0:
            return False
        new = str(x)[::-1]
        if new == str(x):
            return True
        return False


if __name__ == '__main__':
    x = -121
    print(Solution().isPalindrome(x))
