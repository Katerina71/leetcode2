class Solution:
    def mySqrt(self, x: int) -> int:
        if x == 1:
            return 1
        s = x >> 1
        while s * s > x:
            s = (s + x // s) >> 1
        return s


if __name__ == '__main__':
    print(Solution().mySqrt(6))
