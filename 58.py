class Solution:
    def lengthOfLastWord(self, s: str) -> int:
        lst = s.split()
        if not lst:
            return 0
        length = len(lst[-1])
        return length


if __name__ == '__main__':
    print(Solution().lengthOfLastWord("Hello World"))